/*
 * Input_Output.h
 *
 *  Created on: 24 sept. 2018
 *      Author: Michael
 */


#ifndef INPUT_OUTPUT_H_
#define INPUT_OUTPUT_H_

void modoPin(int pin,short int modo);
void Escritura(int pin,short int estado);
int Lectura(int pin);

#endif /* INPUT_OUTPUT_H_ */
