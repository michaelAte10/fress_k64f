/*
 * Copyright 2016-2018 NXP Semiconductor, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * o Redistributions of source code must retain the above copyright notice, this list
 *   of conditions and the following disclaimer.
 *
 * o Redistributions in binary form must reproduce the above copyright notice, this
 *   list of conditions and the following disclaimer in the documentation and/or
 *   other materials provided with the distribution.
 *
 * o Neither the name of NXP Semiconductor, Inc. nor the names of its
 *   contributors may be used to endorse or promote products derived from this
 *   software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
 
/**
 * @file    Colas.c
 * @brief   Application entry point.
 */
#include <stdio.h>
#include "board.h"
#include "peripherals.h"
#include "pin_mux.h"
#include "clock_config.h"
#include "MK64F12.h"
#include "fsl_debug_console.h"
#include "Input_Output.h"
#include <segundos.h>
#include <Teclado.h>

#define OUTPUT 1
#define INPUT 0

#define tam 8
/* TODO: insert other include files here. */

/* TODO: insert other definitions and declarations here. */

/*
 * @brief   Application entry point.
 */
int main(void) {

	int filas[4] = {19,18,9,23};
	int columnas[4] = {11,10,3,2};
	char tecla;
	unsigned valor;
	unsigned tiempo;
	unsigned tiempoAnt = 0;

	int bandera2 = 0;
	int bandera = 1;
	int real;
	int count = 0;

	enum opciones{
		leer,
		mostrar,
		apagado,
	};
	enum opciones opciones = apagado;

    init_rtc(); //clock

    for(int i=0;i<4;i++){
    	modoPin(columnas[i],OUTPUT);  // declaramos salidas //
    	modoPin(filas[i],INPUT);     //declaramos entradas//
    }

    modoPin(22,OUTPUT);

  	/* Init board hardware. */
    BOARD_InitBootPins();
    BOARD_InitBootClocks();
    BOARD_InitBootPeripherals();
  	/* Init FSL debug console. */
    BOARD_InitDebugConsole();


    while(1){
    	tiempo = conversion();
    	tecla = multiplexacion(columnas,filas);
    	real = (int)(tecla - 48);
    	if((real > 0) & (real <= 9)) {
    		Insertar(real);
    		count++;
    	}
    	if((count != 0) & (bandera == 1)) opciones  = leer;
    	switch(opciones){
    	case leer:
    		valor = lectura();
    		bandera = 0;
    		count--;
    		tiempoAnt = tiempo;
    		opciones  = mostrar;
    		break;

    	case mostrar:
    		 Escritura(22,1);
    		if((tiempo - tiempoAnt) >= valor){
    			Escritura(22,0);
    			tiempoAnt = tiempo;
                opciones = apagado;
    		}
    		break;
    	case apagado:
			Escritura(22,0);
    		if((tiempo - tiempoAnt) >= 3){
        		bandera = 1;
    		}
    		break;
    	}


    }
    /* Force the counter to be placed into memory. */
    volatile static int i = 0 ;
    /* Enter an infinite loop, just incrementing a counter. */
    while(1) {
        i++ ;
    }
    return 0 ;
}
