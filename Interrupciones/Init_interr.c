/*
 * Init_interr.c
 *
 *  Created on: 26 sept. 2018
 *      Author: Michael
 */
#include <stdio.h>
#include "fsl_debug_console.h"
#include "MK64F12.h"

void modoPin(void){

	SIM->SCGC5 |= SIM_SCGC5_PORTC_MASK; // prendemos el puerto C
    PORTC->PCR[6] = PORT_PCR_MUX(1) | PORT_PCR_IRQC(9)|PORT_PCR_ISF_MASK | PORT_PCR_PE_MASK | PORT_PCR_PS_MASK; // activamos el puerto C en modo gpio
	NVIC_EnableIRQ(PORTC_IRQn); // Habilitacion de interrupcion de segundos
}

__attribute__((interrupt)) void PORTC_IRQHandler(void)
{
	PORTC->PCR[6] |= PORT_PCR_ISF_MASK;
	printf("pulsador\n");
}
