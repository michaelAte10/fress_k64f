/*
 * Teclado.c
 *
 *  Created on: 19 sept. 2018
 *      Author: Michael
 */
#include <stdio.h>
#include "fsl_debug_console.h"
#include "MK64F12.h"
#include "Input_Output.h"
#define tam 4
int matriz[tam][tam];
int matrizAnt[tam][tam];
char caracteres[tam][tam] = {
		{'1','2','3','A'},
		{'4','5','6','B'},
		{'7','8','9','C'},
		{'*','0','#','D'},
};
char multiplexacion(int multiplexadores[],int columnas[]){
	char t = 'n'; // ponemos un caso que no se de
	for(int i=0;i< tam ;i++){
		// en este for interno realizamos la multiplexacion por cada estado del for externo //
		for(int j=0;j<tam;j++){
			if(i == j)Escritura(multiplexadores[j],1);
			else Escritura(multiplexadores[j],0);
		}
		// una vez definida las variables de estado podemos leer las filas //
        for(int k=0;k<tam;k++){
        	matriz[k][i] = Lectura(columnas[k]);
        	if((matriz[k][i] == 0) & (matrizAnt[k][i] == 1)) {
        		//printf("%c\n",caracteres[i][k]);
        		t = caracteres[i][k];
        	}
        	matrizAnt[k][i] = matriz[k][i];
        }
	}
	return t;
}

