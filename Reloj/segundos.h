/*
 * control.h
 *
 *  Created on: 18 sept. 2018
 *      Author: Michael
 */

#ifndef SEGUNDOS_H_
#define SEGUNDOS_H_

void init_rtc(void);
void conversion(int *vector_time);

#endif /* SEGUNDOS_H_ */
