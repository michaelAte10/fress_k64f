#include <stdio.h>
#include "board.h"
#include "peripherals.h"
#include "pin_mux.h"
#include "clock_config.h"
#include "MK64F12.h"
#include "fsl_debug_console.h"
#include "Input_Output.h"
#include <segundos.h>
#include <Teclado.h>

#define OUTPUT 1
#define INPUT 0



int main(void) {

	int horas = 0;
	int minutos= 0;
	int segundos = 0;

	int Ant = 0;
	short int pos = 0;

	short int bandera_Alarma = 0;

    int valores[3];
	int filas[4] = {19,18,9,23};
	int columnas[4] = {11,10,3,2};
	int Alarma[6]={0,0,0,0,0,0}; // indica que no hay alarma
	char tecla;

	enum digito{
		hd,
		hu,
		md,
		mu,
		sd,
		su,
	};
	enum digito digito = hd;

	enum clock{
		mostrar,
		programar,
		sonar,
	};
	enum clock clock = mostrar;

  	/* Init board hardware. */
    BOARD_InitBootPins();
    BOARD_InitBootClocks();
    BOARD_InitBootPeripherals();
  	/* Init FSL debug console. */
    BOARD_InitDebugConsole();

    init_rtc(); //clock

    modoPin(22,OUTPUT);

    for(int i=0;i<4;i++){
    	modoPin(columnas[i],OUTPUT);  // declaramos salidas //
    	modoPin(filas[i],INPUT);     //declaramos entradas//
    }

    while(1){
    	tecla = multiplexacion(columnas,filas);
    	switch(clock){
    	case mostrar:
    	    conversion(valores);
    	    if(valores[2] != Ant){
    	        printf("%d:%d:%d\n",valores[0],valores[1],valores[2]);
    	        Ant = valores[2];
    	    }
        	if(tecla == '*') {
        		printf("..........Alarma Pasada..........");
        		printf("%d:%d:%d\n",horas,minutos,segundos);
        		printf("......programe la alarma......\n");
        		clock = programar;
        	}
        	else if((valores[0] == horas)&(valores[1]==minutos)&(valores[2]==segundos)& (bandera_Alarma == 1)) clock = sonar;
    		break;
    	case programar:
    		bandera_Alarma = 1;
            switch(digito){
            case hd:
            	if((tecla>47)&(tecla<51)) {
            		Alarma[pos] = (int)(tecla - 48);
            		printf("%d",Alarma[pos]);
            	}
            	else if(tecla == '#') {
            		printf("-");
            		pos++;
            		digito = hu;
            	}
            	break;
            case hu:
            	if((tecla>47)&(tecla<52)) {
            	    Alarma[pos] = (int)(tecla - 48);
            	    printf("%d",Alarma[pos]);
            	}
            	else if(tecla == '#') {
            		pos++;
            	    printf(" : ");
            	    digito = md;
            	}
            	break;

            case md:
            	if((tecla>47)&(tecla<54)){
            		Alarma[pos] = (int)(tecla - 48);
            		printf("%d",Alarma[pos]);
            	}
            	else if(tecla == '#') {
            		printf("-");
            		pos++;
            		digito = mu;
            	}
            	break;
            case mu:
            	if((tecla>47)&(tecla<58)){
            		Alarma[pos] = (int)(tecla - 48);
            		printf("%d",Alarma[pos]);
            	}
            	else if(tecla == '#') {
            		pos++;
            		printf(" : ");
            		digito = sd;
            	}
            	break;

            case sd:
            	if((tecla>47)&(tecla<54)) {
            		Alarma[pos] = (int)(tecla - 48);
            		printf("%d",Alarma[pos]);
            	}
            	else if(tecla == '#') {
            		printf("-");
            		pos++;
                    digito = su;
            	}
            	break;

            case su:
            	if((tecla>47)&(tecla<58)){
            		Alarma[pos] = (int)(tecla - 48);
            		printf("%d",Alarma[pos]);
            	}
            	else if(tecla == '#') {
            		pos = 0;
            		horas = Alarma[0]*10 + Alarma[1];
            		minutos = Alarma[2]*10 + Alarma[3];
            		segundos = Alarma[4]*10 + Alarma[5];
            		printf("\n");
            		printf("..........Alarma Actual..........");
            		printf("%d:%d:%d\n",horas,minutos,segundos);
            		digito = hd;
            		clock = mostrar;
            	}
            	break;
            }
            if(tecla == 'C'){ //aca cancelamos la alarma
            	bandera_Alarma = 0;
            	printf("Alarma deshabilitada\n");
            	clock = mostrar;
            }
            else if(tecla == 'A') clock = mostrar; // nos devolvemos
    		break;
    	case sonar:
             Escritura(22,1);
             if(tecla == 'D') {
            	 Escritura(22,0);
            	 printf("listo ya desperte ;)\n");
            	 clock = mostrar;
             }
    	break;

    	}
   }

    volatile static int i = 0 ;
    while(1) {
        i++ ;
    }
    return 0 ;
}
