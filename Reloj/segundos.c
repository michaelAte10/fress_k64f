/*
 * control.c
 *
 *  Created on: 18 sept. 2018
 *      Author: Michael
 *
 */
#include <stdio.h>
#include "fsl_debug_console.h"
#include "MK64F12.h"

int tiempo = 0; // tiempo inicial
void init_rtc(){

	SIM->SCGC6 |= SIM_SCGC6_RTC_MASK; // Prendemos modulo clock
	RTC->SR  =  ~RTC_SR_TCE_MASK; // bandera deshabilitamos interrupcion
	RTC->CR  =  RTC_CR_OSCE_MASK;// activamos
	RTC->TSR = 1;// donde inicia el tiempo
	RTC->TAR = 5;// alarma
	RTC->IER =  RTC_IER_TSIE_MASK; //
	RTC->SR  =  RTC_SR_TCE_MASK; // bandera habilitar interrupcion
	NVIC_EnableIRQ(RTC_Seconds_IRQn); // Habilitacion de interrupcion de segundos
}
void conversion(int *vector_time){
	vector_time[0] =  tiempo / 3600;		 //  horas
	vector_time[1] = (tiempo % 3600)/60; //  minutos
	vector_time[2] = (tiempo)%60; // segundos
	if(tiempo >= 86399) tiempo = 0;

}
__attribute__((interrupt)) void RTC_Seconds_IRQHandler(void){
	tiempo++;
}

