.data

.equ PTAPCRbase,0x40049000 //direccion base PTA
.equ PTBPCRbase,0x4004A000 //direccion base PTB
.equ PTCPCRbase,0x4004B000 //direccion base PTC
.equ PTDPCRbase,0x4004C000 //direccion base PTD
.equ PTEPCRbase,0x4004D000 //direccion base PTE

.equ SIMSGCSCb,0x40048028 //Direccion SIM_SCGSC0

.equ GPIOAbase,0x400FF000 //direccion base GPIOA
.equ GPIOBbase,0x400FF040 //direccion base GPIOB
.equ GPIOCbase,0x400FF080 //direccion base GPIOC
.equ GPIODbase,0x400FF0C0 //direccion base GPIOD
.equ GPIOEbase,0x400FF100 //direccion base GPIOE

.equ tiempoDesp,10// tiempo de desplazamiento
.equ tiempoMulti,500// tiempo de Mltiplexacion
.equ sizematrix,22

.text

.global matrix

matrix:

init_all:

bl 	PORTBC //Ponemos como salida todos los gpio que podamos del puerto B Y C
bl init_pulsador_a
bl 	InitCaracter

mov R7,#0
ldr	R0,=0X20000000 // memoria base

mov R2,#51
mov R3,#8
strb R3,[R0,R2] //  punto fijo

mov R3,#50
strb R7,[R0,R3] //  contador de cuantas veces se ha desplazado
str R7,[R0,#44]

ldr R2,=#tiempoMulti// tiempo de multiplexacion
str R2,[R0,#60]

//ldr	 R0,=0X20000100
mov  R0,#1
lsl	 R0,#21
add	 R0,#1
lsl	 R0,#8
mov  R1,#tiempoDesp // tiempo de desplazamiento de pantalla
strb R1,[R0,#31] // Segundo contador EL DE pantalla
mov	 R1,#0
strb R1,[R0,#29] // fin o start
b	loop
InitCaracter:
///escribimos el caracter 8x8 que deseamos mostrar, lo escribimos en memoria///
////PONEMOS LAS SALIDAS QUE DESEAMOS EN MEMORIA/////
push {lr} //almacena el return en el stack
//ldr	 R0,=0X20000100
mov  R0,#1
lsl	 R0,#21
add	 R0,#1
lsl	 R0,#8
ldrb R1,[R0,#29]
cmp  R1,#0
beq screen
bne gameover
pop {pc}
gameover:
ldr		R0,=0X20000000 // memoria base

//129, 66, 0, 102, 102, 0, 60, 66;;
mov		R1,#129 // BIT 0
strb	R1,[R0,#0]

mov		R1,#66
strb	R1,[R0,#1]

mov		R1,#0
strb	R1,[R0,#2]

mov		R1,#102
strb	R1,[R0,#3]

mov		R1,#102
strb	R1,[R0,#4]

mov		R1,#0
strb	R1,[R0,#5]

mov		R1,#60
strb	R1,[R0,#6]

mov		R1,#66
strb	R1,[R0,#7]
//0, 0, 0, 160, 64, 224, 64, 0;

pop {pc}
screen:
ldr		R0,=0X20000000 // memoria base

mov 	R2,#50
ldrb	R3,[R0,R2] //cargamos offset
//129, 129, 209, 161, 241, 161, 129, 129;
add		R3,#1
mov		R1,#129 // BIT 0
strb	R1,[R0,R3]

add		R3,#1
mov		R1,#129
strb	R1,[R0,R3]

add		R3,#1
mov		R1,#209
strb	R1,[R0,R3]

add		R3,#1
mov		R1,#161
strb	R1,[R0,R3]

add		R3,#1
mov		R1,#241
strb	R1,[R0,R3]

add		R3,#1
mov		R1,#161
strb	R1,[R0,R3]

add		R3,#1
mov		R1,#129
strb	R1,[R0,R3]

add		R3,#1
mov		R1,#129
strb	R1,[R0,R3]

//129, 129, 149, 137, 157, 137, 129, 129;
mov		R1,#129 // BIT 0
strb	R1,[R0,R3]
//FILA 2/////
add		R3,#1
mov		R1,#129
strb	R1,[R0,R3]
//FILA 3/////
add		R3,#1
mov		R1,#149
strb	R1,[R0,R3]
//FILA 4/////
add		R3,#1
mov		R1,#137
strb	R1,[R0,R3]
//FILA 5/////
add		R3,#1
mov		R1,#157
strb	R1,[R0,R3]
//FILA 6/////
add		R3,#1
mov		R1,#137
strb	R1,[R0,R3]
//FILA 7/////
add		R3,#1
mov		R1,#129
strb	R1,[R0,R3]
//FILA 8/////
add		R3,#1
mov		R1,#129
strb	R1,[R0,R3]

//139, 133, 143, 133, 129, 129, 129, 129;
add		R3,#1
mov		R1,#139 // BIT 0
strb	R1,[R0,R3]

add		R3,#1
mov		R1,#133
strb	R1,[R0,R3]

add		R3,#1
mov		R1,#143
strb	R1,[R0,R3]

add		R3,#1
mov		R1,#133
strb	R1,[R0,R3]

add		R3,#1
mov		R1,#129
strb	R1,[R0,R3]

add		R3,#1
mov		R1,#129
strb	R1,[R0,R3]

add		R3,#1
mov		R1,#129
strb	R1,[R0,R3]

add		R3,#1
mov		R1,#129
strb	R1,[R0,R3]



pop {pc}
PORTBC:
push {lr} //almacena el return en el stack
mov R1,#1
lsl R1,#12
add R1,#1
lsl R1,#3
add R1,#1
lsl R1,#10
add R1,#1
lsl R1,#5
add R1,#8
ldr R0, =0xC00 //PTB oscilador CAP 12.2.12 K64 reference manual SIM
str R0,[R1,#16] //pag 292 SIM_SCGC5 es ahi donde esta la habilitacion de los puertos y habilitamos el B

//////////////////inicia habilitacion de pin deseadado como GPIO////////////////////
//realicemos primero los del puertB//// filas osea positivo
ldr R1, =#PTBPCRbase //activa el PCR generico PTB pag 282
ldr R0, =0x100 //Configuracion seg�n CAP 11.5.1 K64 reference manual PORT selecionamos el modo GPIO
str R0,[R1,#8] //pag 277 PORTB2
str R0,[R1,#12]//PORTB3
str R0,[R1,#40]//PORTB10
str R0,[R1,#44]//PORTB11
str R0,[R1,#36]//PORTB9
str R0,[R1,#72]//PORTB18
str R0,[R1,#76]//PORTB19
str R0,[R1,#80]//PORTB20

///////////////////decidimos si queremos el pin como entrada digital o salida///////////////////////////////
ldr R1, =#GPIOBbase //cargamos en R1 LA BASE del GPIO
// formamos el numero 111000000111000001100 pa no usar ldr
ldr	R0,=#0x1C0E0C
str R0,[R1,#0x14] //DDR Cap 55.2.6 K64 400FF054 Aca declaro el pin como entrada o salida pg1759
/////////////////Realizamos el puertC/////////////////////////columnas osea negativo
ldr R1, =#PTCPCRbase //activa el PCR generico PTB pag 282
ldr R0, =0x100 //Configuracion seg�n CAP 11.5.1 K64 reference manual PORT selecionamos el modo GPIO
str R0,[R1,#4] //PORTC1 COLM1
str R0,[R1,#44]//PORTC11 COLM2
str R0,[R1,#40]//PORTC10 COLM3
str R0,[R1,#20]//PORTC5
str R0,[R1,#28]//PORTC7
str R0,[R1]//PORTC0
str R0,[R1,#36]//PORTC9
str R0,[R1,#32]//PORTC8
str R0,[R1,#24]//PORTC6 // ENTRADA PULSADOR

ldr R1, =#GPIOCbase //cargamos en R1 LA BASE del GPIO

//FORMAMOS EL FA3
ldr	R0,=#0xfa3

str R0,[R1,#0x14] //DDR Cap 55.2.6 K64 400FF054 Aca declaro el pin como entrada o salida pg1759
pop {pc}

init_pulsador_a:

push {lr} //almacena el return en el stack

ldr R1, =SIMSGCSCb //carga la direccion del SIMSGC base
ldr R0, =0x200 //PTC oscilador CAP 12.2.12 K64 reference manual SIM portA
ldr R2,[R1,#16] //leer offset SIMSCGSC0 + 16 = SIMSCGSC5
orr R0,R2,R0 //OR para encender el bit que encesito sin alterar los demas
str R0,[R1,#16] //Offset es SIMSCGSC5
ldr R1, =#PTAPCRbase //activa el PCR generico PTa
ldr R0, =0x100 //Configuracion seg�n CAP 11.5.1 K64 reference manual PORT (Pin configurado como GPIO)
str R0,[R1,#16] //Bit 4 = (4*4 = 16)<= offset
ldr R1, =#GPIOAbase //activa el GPIOC
mov R0, #1 //Bit 0 en 1, los demas en 0
lsl R0,#6 //Bit 6 en 1, los demas en 0
mvn R0,R0 //niega los bits. Todos son uno menos el bit 6 que es cero

ldr R2,[R1,#0x14] //leer offset
and R0,R2,R0 //OR para encender el bit que encesito sin alterar los demas
str R0,[R1,#0x14] //DDR Cap 55.2.6 K64 reference manual GPIO
pop {pc}

leer:
push {lr}

ldr R4, =0x20000100 //apuntar a la memoria 0x2000_0000
ldr R5, =#GPIOCbase //activa el GPIOC
ldr R0,[R5,#16] //leer el GPIOC_PDIR y ponerlo en R0
str R0,[R4] //almacenar la lectura en la memoria 0000

pop {pc}

leer_2:

push {lr}

ldr R4, =0x20000100 //apuntar a la memoria 0x2000_0000

ldr R5, =#GPIOAbase //activa el GPIOC
ldr R0,[R5,#16] //leer el GPIOC_PDIR y ponerlo en R0
mov R5,#16
and R0,R5,R0
str R0,[R4,#36] //almacenar la lectura en la memoria 0000

pop {pc}
procesar:
push {lr}
bl estado_puls
pop {pc}

estado_puls:

ldr R4, =0x20000100 //variable de estados
ldrb R3,[R4,#20] //lee la variable del estado
cmp R3, #1 //la compara con 1
beq estado_uno //Si es un uno salte a estado_en_uno
b   estado_dos //si es un cero salte a otro

estado_uno: //estado leds apagados. Si se presiona el pulsador se pasa al estado uno
ldr R4, =0x20000100
ldr R1, [R4,#36] //lectura de pulsador almacenada se lleva a R1
mov R0, #1 //carga un uno
lsl R0, #4 //lo vuelve el bit 6
and R0, R1, R0 //hace una and con el dato leido. Pulsador esta conectado al bit 6 del pt
beq s1_a_s2 //si R0 se convierte en 0, entonces R0 est� presionado
bx lr //Si R0 es uno, el pulsados no esta presionado y regresa
s1_a_s2:
ldr R4, =0x20000100 //variable de estados
mov R0, #2 //cambia el estado a dos
strb R0,[R4,#20]
bx lr

estado_dos:
ldr R4, =0x20000100
ldr R1, [R4,#36] //lectura de pulsador almacenada se lleva a R1
mov R0, #16 //carga un uno
and R0, R1, R0 //hace una and con el dato leido. Pulsador esta conectado al bit 6 del pt
bne s2_a_s1 //si R0 se convierte en 1, entonces R0 no est� presionado
bx lr //Si R0 es cero, el pulsados esta presionado y regres
s2_a_s1:
ldr r4, =0x20000100 //variable de estados
mov R2,#51
mov R0, #1 //cambia el estado a tres
strb R0,[R4,#20]
ldr  r4, =0x20000000
ldrb R6,[r4,R2]
lsl R6,#1 // Cuando entra a este estado se da la suma de la variable
strb R6,[r4,R2]

bx lr

procesar2:
push {lr}
bl estado_puls2
pop {pc}
estado_puls2:

ldr R4, =0x20000100 //variable de estados
ldrb R3,[R4,#16] //lee la variable del estado
cmp R3, #1 //la compara con 1
beq estado_uno2 //Si es un uno salte a estado_en_uno
b   estado_dos2 //si es un cero salte a otro

estado_uno2: //estado leds apagados. Si se presiona el pulsador se pasa al estado uno
ldr R4, =0x20000100
ldr R1, [R4] //lectura de pulsador almacenada se lleva a R1
mov R0, #1 //carga un uno
lsl R0, #6 //lo vuelve el bit 6
and R0, R1, R0 //hace una and con el dato leido. Pulsador esta conectado al bit 6 del pt
beq s1_a_s22 //si R0 se convierte en 0, entonces R0 est� presionado
bx lr //Si R0 es uno, el pulsados no esta presionado y regresa
s1_a_s22:
ldr R4, =0x20000100 //variable de estados
mov R0, #2 //cambia el estado a dos
strb R0,[R4,#16]
bx lr
estado_dos2:
ldr R4, =0x20000100
ldr R1, [R4] //lectura de pulsador almacenada se lleva a R1
mov R0, #1 //carga un uno
lsl R0, #6 //lo vuelve el bit 6
and R0, R1, R0 //hace una and con el dato leido. Pulsador esta conectado al bit 6 del pt
bne s2_a_s12 //si R0 se convierte en 1, entonces R0 no est� presionado
bx lr //Si R0 es cero, el pulsados esta presionado y regres
s2_a_s12:
ldr R4, =0x20000100 //variable de estados
mov R0, #1 //cambia el estado a tres
strb R0,[R4,#16]

ldr r4, =0x20000000
mov R2,#51
ldrb R6,[r4,R2]
lsr R6,#1 // Cuando entra a este estado se da la suma de la variable
strb R6,[r4,R2]
bx lr
Resetcount:
push {lr}
ldr	R0,=0X20000000 // memoria base
mov R1,#0
str R1,[R0,#44] // contador de multiplexacion
pop {pc}
loop:
bl  leer
bl  procesar2
bl  leer_2
bl  procesar
bl  matriz
b	loop
matriz: // vamos a barrer por fila
push {lr} //almacena el return en el stack
ldr	R0,=0X20000000 // memoria base

ldr R1,[R0,#44]
add R1,#1
str R1,[R0,#44]

ldr R2,[R0,#60]
cmp R1,R2// aca cambiamos el 2 por una variable generica
beq	seleccion
pop {pc}

seleccion:
add R7,#1
cmp	R7,#1
beq	std_1
cmp	R7,#2
beq	std_2
cmp	R7,#2
beq	std_2
cmp	R7,#3
beq	std_3
cmp	R7,#4
beq	std_4
cmp	R7,#5
beq	std_5
cmp	R7,#6
beq	std_6
cmp	R7,#7
beq	std_7
cmp	R7,#8
beq	std_8
b	loop

std_1:
/// activo columnas necesarias (salidas)///////
mov	R6,#0 // Offset
bl	controlbits
ldr	R1, =#GPIOCbase //cargamos en r1 la base del gpioc

ldr R0,[R1,#0x14] //clear pcor
str R0,[R1,#8] //clear pcor

mvn	R5,R5
str R5,[R1,#4] //SET pdor

//////FILA///////// variable de estado
ldr R1, =#GPIOBbase //cargamos en R1 LA BASE del GPIOb
//debo limpiar la memoria del estado que venga//
ldr R0,[R1,#0x14] //clear pcor
str R0,[R1,#8] //clear pcor
/////enciendo fila////////
mov	R0,#1
lsl	R0,#2 // LO CORRO SEGUN EL BIT QUE QUIERA
str R0,[R1,#4] //SET pdor

bl Resetcount
pop {pc}
std_2:
/// activo columnas necesarias (salidas)///////
mov	R6,#1 // Offset
bl	controlbits
ldr	R1, =#GPIOCbase //cargamos en r1 la base del gpioc

ldr R0,[R1,#0x14] //clear pcor
str R0,[R1,#8] //clear pcor

mvn	R5,R5
str R5,[R1,#4] //SET pdor
//////FILA///////// variable de estado
ldr R1, =#GPIOBbase //cargamos en R1 LA BASE del GPIOb
//debo limpiar la memoria del estado que venga//
ldr R0,[R1,#0x14] //clear pcor
str R0,[R1,#8] //clear pcor
/////enciendo fila////////
mov	R0,#1
lsl	R0,#3 // LO CORRO SEGUN EL BIT QUE QUIERA
str R0,[R1,#4] //SET pdor
bl Resetcount
pop {pc}
std_3:

/// activo columnas necesarias (salidas)///////
mov	R6,#2 // Offset
bl	controlbits
ldr	R1, =#GPIOCbase //cargamos en r1 la base del gpioc
ldr R0,[R1,#0x14] //clear pcor
str R0,[R1,#8] //clear pcor

mvn	R5,R5
str R5,[R1,#4] //SET pdor
//////FILA///////// variable de estado
ldr R1, =#GPIOBbase //cargamos en R1 LA BASE del GPIOb
//debo limpiar la memoria del estado que venga//
ldr R0,[R1,#0x14] //clear pcor
str R0,[R1,#8] //clear pcor
/////enciendo fila////////
mov	R0,#1
lsl	R0,#10 // LO CORRO SEGUN EL BIT QUE QUIERA
str R0,[R1,#4] //SET pdor

bl Resetcount

pop {pc}
std_4:
/// activo columnas necesarias (salidas)///////
mov	R6,#3 // Offset
bl	controlbits
ldr	R1, =#GPIOCbase //cargamos en r1 la base del gpioc

ldr R0,[R1,#0x14] //clear pcor
str R0,[R1,#8] //clear pcor

mvn	R5,R5
str R5,[R1,#4] //SET pdor

//////FILA///////// variable de estado
ldr R1, =#GPIOBbase //cargamos en R1 LA BASE del GPIOb
//debo limpiar la memoria del estado que venga//
ldr R0,[R1,#0x14] //clear pcor
str R0,[R1,#8] //clear pcor
/////enciendo fila////////
mov	R0,#1
lsl	R0,#11 // LO CORRO SEGUN EL BIT QUE QUIERA
str R0,[R1,#4] //SET pdor

bl Resetcount

pop {pc}
std_5:
//////FILA///////// variable de estado
mov	R6,#4 // Offset
bl	controlbits
/// activo columnas necesarias (salidas)///////

ldr	R1, =#GPIOCbase //cargamos en r1 la base del gpioc

ldr R0,[R1,#0x14] //clear pcor
str R0,[R1,#8] //clear pcor

mvn	R5,R5
str R5,[R1,#4] //SET pdor

ldr R1, =#GPIOBbase //cargamos en R1 LA BASE del GPIOb
//debo limpiar la memoria del estado que venga//
ldr R0,[R1,#0x14] //clear pcor
str R0,[R1,#8] //clear pcor
/////enciendo fila////////
mov	R0,#1
lsl	R0,#9 // LO CORRO SEGUN EL BIT QUE QUIERA
str R0,[R1,#4] //SET pdor

bl Resetcount

pop {pc}
std_6:
/// activo columnas necesarias (salidas)///////
mov	R6,#5 // Offset
bl	controlbits
ldr	R1, =#GPIOCbase //cargamos en r1 la base del gpioc

ldr R0,[R1,#0x14] //clear pcor
str R0,[R1,#8] //clear pcor

mvn	R5,R5
str R5,[R1,#4] //SET pdor
//////FILA///////// variable de estado
ldr R1, =#GPIOBbase //cargamos en R1 LA BASE del GPIOb
//debo limpiar la memoria del estado que venga//
ldr R0,[R1,#0x14] //clear pcor
str R0,[R1,#8] //clear pcor
/////enciendo fila////////
mov	R0,#1
lsl	R0,#18 // LO CORRO SEGUN EL BIT QUE QUIERA
str R0,[R1,#4] //SET pdor

bl Resetcount
pop {pc}
std_7:
/// activo columnas necesarias (salidas)///////
mov	R6,#6 // Offset
bl	controlbits
ldr	R1, =#GPIOCbase //cargamos en r1 la base del gpioc

ldr R0,[R1,#0x14] //clear pcor
str R0,[R1,#8] //clear pcor

mvn	R5,R5
str R5,[R1,#4] //SET pdor
//////FILA///////// variable de estado
ldr R1, =#GPIOBbase //cargamos en R1 LA BASE del GPIOb
//debo limpiar la memoria del estado que venga//
ldr R0,[R1,#0x14] //clear pcor
str R0,[R1,#8] //clear pcor
/////enciendo fila////////
mov	R0,#1
lsl	R0,#19 // LO CORRO SEGUN EL BIT QUE QUIERA
str R0,[R1,#4] //SET pdor
bl Resetcount
pop {pc}
std_8:
/// activo columnas necesarias (salidas)///////
ldr	R0,=0X20000000 // memoria base
mov	R6,#7 // Offset
mov R2,#51
ldrb R3,[R0,R2]
beq  FIN

bl	controlbits
mov R3,R5 //Guardamos la respuesta anterior en R3

mov R6,#51 // ahi esta el punto que deseamos escribir
bl  controlbits

orr R5,R5,R3
cmp R5,R3
beq FIN
vuelvo:

ldr	R1, =#GPIOCbase //cargamos en r1 la base del gpioc

ldr R0,[R1,#0x14] //clear pcor
str R0,[R1,#8] //clear pcor

mvn	R5,R5 // proviene de Controlbits
str R5,[R1,#4] //SET pdor
//////FILA///////// variable de estado
ldr R1, =#GPIOBbase //cargamos en R1 LA BASE del GPIOb
//debo limpiar la memoria del estado que venga//
ldr R0,[R1,#0x14] //clear pcor
str R0,[R1,#8] //clear pcor
/////enciendo fila////////
mov	R0,#1
lsl	R0,#20 // LO CORRO SEGUN EL BIT QUE QUIERA
str R0,[R1,#4] //SET pdor
bl Count_2
bl InitCaracter

bl Resetcount
mov	R7,#0
b loop

FIN:
mov  R0,#1
lsl	 R0,#21
add	 R0,#1
lsl	 R0,#8
mov  R1,#1
strb R1,[R0,#29]
ldr		R0,=0X20000000  // memoria base
mov  R1,#0
mov  R2,#51
strb R1,[R0,R2]
mov  R1,#3
mov  R2,#50
strb R1,[R0,R2]
b   vuelvo
Count_2:
push {lr}
ldr 	R0,=0X20000100  // memoria base de count2
ldrb    R1,[R0,#31]
add		R1,#-1
strb    R1,[R0,#31]
beq		Offset
aux9:
pop {pc}
Offset:
ldr		R0,=0X20000000 // memoria base
mov 	R2,#50
ldrb	R1,[R0,R2] // leemos estado de contador de cuantas veces se ha desplazado
bl		desplazamiento
auxretorn:
add		R1,#1
cmp		R1,#sizematrix
beq		rffset
aux10:
mov 	R2,#50
strb	R1,[R0,R2]
ldr		R0,=0X20000100  // memoria base
mov 	R1,#tiempoDesp
strb 	R1,[R0,#31] // Segundo contador EL DE pantallas
b		aux9
rffset:
mov	R1,#0
b aux10
desplazamiento:
push {lr}
// bit 7 en 0
mov	 R4,#sizematrix
add	 R4,#-1
ldrb R2,[R0,R4]
strb R2,[R0,#0]
cmp R1,#0
beq	auxretorn
// bit 8 en 1
add	 R4,#1
ldrb R2,[R0,R4]
strb R2,[R0,#1]
cmp R1,#1
beq	auxretorn
// bit 9 en 2
add	 R4,#1
ldrb R2,[R0,R4]
strb R2,[R0,#2]
cmp R1,#2
beq	auxretorn
// bit 10 en 3
add	 R4,#1
ldrb R2,[R0,R4]
strb R2,[R0,#3]
cmp R1,#3
beq	auxretorn
// bit 11 en 4
add	 R4,#1
ldrb R2,[R0,R4]
strb R2,[R0,#4]
cmp R1,#4
beq	auxretorn
// bit 12 en 5
add	 R4,#1
ldrb R2,[R0,R4]
strb R2,[R0,#5]
cmp R1,#5
beq	auxretorn
// bit 13 en 6
add	 R4,#1
ldrb R2,[R0,R4]
strb R2,[R0,#6]
cmp R1,#6
beq	auxretorn
// bit 14 en 7
add	 R4,#1
ldrb R2,[R0,R4]
strb R2,[R0,#7]
pop {pc}
c1:
bfi	R5,R1,#8,#1 // Ponemos una posicion de bit en alto de r5
b aux
c2:
bfi	R5,R1,#9,#1 // Ponemos una posicion de bit en alto de r5
b aux2
c3:
bfi	R5,R1,#0,#1 // Ponemos una posicion de bit en alto de r5
b aux3
c4:
bfi	R5,R1,#7,#1 // Ponemos una posicion de bit en alto de r5
b aux4
c5:
bfi	R5,R1,#5,#1 // Ponemos una posicion de bit en alto de r5
b aux5
c6:
bfi	R5,R1,#10,#1 // Ponemos una posicion de bit en alto de r5
b aux6
c7:
bfi	R5,R1,#11,#1 // Ponemos una posicion de bit en alto de r5
b aux7
c8:
bfi	R5,R1,#1,#1 // Ponemos una posicion de bit en alto de r5
b aux8
controlbits:
push {lr} //almacena el return en el stack
ldr	R0,=0X20000000 // memoria base
mov	R1,#1
mov	R5,#0
ldrb 	R4,[R0,R6]
mov	R2,#1
and	R2,R4,R2 // Si es cero no le monte nada si es uno montele un uno en el bit que le activa esa columna
cmp	R2,#0
bne	c1
aux:
mov	R2,#2
and	R2,R4,R2
cmp	R2,#0
bne	c2
aux2:
mov	R2,#4
and	R2,R4,R2
cmp	R2,#0
bne	c3
aux3:
mov	R2,#8
and	R2,R4,R2
cmp	R2,#0
bne	c4
aux4:
mov	R2,#16
and	R2,R4,R2
cmp	R2,#0
bne	c5
aux5:
mov	R2,#32
and	R2,R4,R2
cmp	R2,#0
bne	c6
aux6:
mov	R2,#64
and	R2,R4,R2
cmp	R2,#0
bne	c7
aux7:
mov	R2,#128
and	R2,R4,R2
cmp	R2,#0
bne	c8
aux8:
pop {pc}
