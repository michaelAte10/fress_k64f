/*
 * adc.c
 *
 *  Created on: 23 oct. 2018
 *      Author: Michael
 */

#include "MK64F12.h"

void init(void){
	SIM->SCGC6 |= SIM_SCGC6_ADC0(1);
	ADC0->CFG1 = ADC_CFG1_ADLPC_MASK | ADC_CFG1_ADICLK(3) | ADC_CFG1_MODE(3) | ADC_CFG1_ADICLK_MASK;
}
int convert_sinint(void){ // sin interrupcion
	ADC0->SC1[0] = ADC_SC1_AIEN(0) | ADC_SC1_ADCH(0) | ADC_SC1_DIFF(0);
	while((ADC0->SC1[0] & ADC_SC1_COCO_MASK) == 0);
	return ADC0->R[0];
}
void convert_conint(void){ // con interrupcion

}

