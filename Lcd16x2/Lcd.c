#include <stdio.h>
#include "board.h"
#include "peripherals.h"
#include "pin_mux.h"
#include "clock_config.h"
#include "MK64F12.h"
#include "fsl_debug_console.h"
#include "lcdlibreria.h"
#include "adc.h"
#include "senales.h"
#include "timer.h"
#include "math.h"
#include <string.h>
#include <pwm.h>


void f_a(float n, char *res, int afterpoint);
int main(void) {
  	/* Init board hardware. */
    BOARD_InitBootPins();
    BOARD_InitBootClocks();
    BOARD_InitBootPeripherals();
  	/* Init FSL debug console. */
    BOARD_InitDebugConsole();
    init_pit0_timer();
    Init_lcd();
    init(); // conversor ADC
    init_dac(); // conversor DAC
    Escribir_lcd("MAX:",0,0);
    Escribir_lcd("MIN:",0,1); // a partir de 40 se para a fila 2

     //Escribir_lcd("10");
    /* Force the counter to be placed into memory. */
    volatile static int i = 0 ;
    /* Enter an infinite loop, just incrementing a counter. */
    char buffer[5] = "";
    //float y;
    float valores[2];
    float maxi,mini;
    int  analog;
    while(1) {
    	analog = convert_sinint();
    	set_value(analog/16);


        maxmin(analog,valores);

        maxi = (valores[0]/65536)*3.3;
        mini = (valores[1]/65536)*3.3;

    	f_a(maxi,buffer,2);
    	Escribir_lcd(buffer,5,0);

    	f_a(mini,buffer,2);
    	Escribir_lcd(buffer,5,1);

    	i++ ;
    	//delay(10);
    }
    return 0 ;
}
void f_a(float n, char *res, int cifras)
{
    //sacamos la parte entera
	char numdeci[cifras];
    int ipart = (int)n;
    // sacamos la parte flotante
    float fpart = n - (float)ipart;
    int i_fpart;
    sprintf(res,"%d",ipart);
    if (cifras != 0)
    {
        strcat(res,"."); // lo concatenamos con el .
        fpart = fpart * pow(10, cifras); // buscamos su parte entera
        i_fpart = abs((int)fpart); // sacamos su parte entera
        sprintf(numdeci,"%d",i_fpart);// lo volvemos una cadena
        strcat(res,numdeci);//concatenamos ambas cadenas
    }
}
