/*
 * timer.h
 *
 *  Created on: 25 oct. 2018
 *      Author: Michael
 */

#ifndef TIMER_H_
#define TIMER_H_

void init_pit0_timer();
void delay(long unsigned retardo);

#endif /* TIMER_H_ */
