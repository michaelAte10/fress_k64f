/*
 * lcdlibreria.h
 *
 *  Created on: 11 oct. 2018
 *      Author: Michael
 */

#ifndef LCDLIBRERIA_H_
#define LCDLIBRERIA_H_

void modo(short int IO);
void write_lcd(char dato,short int rsf,short int size);
void Escribir_lcd(char *p,uint8_t pos,uint8_t fila);
void clear_lcd(void);
void Init_lcd(void);

#endif /* LCDLIBRERIA_H_ */
