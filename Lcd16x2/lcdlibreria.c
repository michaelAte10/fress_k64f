/*
 * lcdlibreria.c
 *
 *  Created on: 11 oct. 2018
 *      Author: Michael
 */
#include <stdio.h>
#include "fsl_debug_console.h"
#include "MK64F12.h"
#include "Input_Output.h"
#include "timer.h"


#define OUTPUT 1
#define INPUT 0
#define rs 19
#define rw 18
#define e  9

#define d7 2
#define d6 3
#define d5 10
#define d4 11

int pines[4] = {d7,d6,d5,d4};

void modo(short int IO){
 if(IO == 1){
    for(int i=0;i<4;i++){
    	modoPin(pines[i],OUTPUT);  // declaramos salidas  d4,d5,d6,d7//
    }
  }
 else {
	 for(int i=0;i<4;i++){
	    modoPin(pines[i],INPUT);  // declaramos entradas d4,d5,d6,d7//
	}
  }
}
void write_lcd(char dato,short int rsf,short int size){
	int mascara;
	int tem;
	modo(OUTPUT);
	if(rsf == 1)Escritura(rs,1);// es un dato
	else if(rsf == 0)Escritura(rs,0); // es un comando
	if(size == 1) mascara = 0x08;
	else if(size == 2) mascara = 0x80;
	for(int j = 0 ;j<size;j++){
		Escritura(e,1); // indica que vamos a enviar el dato a escribir en este momento
		for(int i = 0;i<4;i++){
			tem = dato & mascara;
			mascara = mascara>>1;
			Escritura(pines[i],(tem)?1:0);
		}
		Escritura(e,0); // indica que vamos a enviar el dato a escribir en este momento
	}
	modo(INPUT);
}
void Escribir_lcd(char *p,uint8_t pos,uint8_t fila){
	if(fila == 1) write_lcd(0b11000000 + pos,0,2);
	else if(fila == 0)write_lcd(0x80 + pos,0,2);
	while(*p){
		write_lcd(*p,1,2);
		p++; // aumentamos la direccion
	}
}
void Init_lcd(void){
	modoPin(rs,OUTPUT); // rs
	modoPin(rw,OUTPUT); // rw
	modoPin(e,OUTPUT); // e
	Escritura(rw,0); // indica modo de escribir
	Escritura(e,0); // indica que vamos a enviar el dato a escribir en este momento
    delay(40);
	write_lcd(0x03,0,1);
	delay(15);
	write_lcd(0x03,0,1);
    delay(5);
	write_lcd(0x03,0,1);
	delay(5);
	write_lcd(0x02,0,2);
	delay(5);
	write_lcd(0b1100,0,2);
	delay(1);
	write_lcd(0x06,0,2);
	write_lcd(0x80,0,2);
    write_lcd(0b00000001,0,2); //clear
    while(Lectura(d7)!=0);
    delay(100);
}
void clear_lcd(void){
    write_lcd(0b00000001,0,2); //clear
    while(Lectura(d7)!=0);
}





