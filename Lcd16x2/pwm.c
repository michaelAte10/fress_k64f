/*
 * pwm.c
 *
 *  Created on: 30 oct. 2018
 *      Author: Michael
 */
#include "MK64F12.h"

#define MK64F12

void init_dac(void) {
#if defined(MK64F12)
SIM->SCGC2=SIM_SCGC2_DAC0_MASK | SIM_SCGC6_DAC0_MASK;
//PORT_PCR no necesario en K64
#endif

#if defined(MKL25Z)
SIM->SCGC6=SIM_SCGC6_DAC0_MASK;
//PTE20 ya es DAC por default

#endif


DAC0->C0 = DAC_C0_DACEN_MASK | DAC_C0_DACRFS(1) | DAC_C0_DACTRGSEL(1);
DAC0->C1 = 0;

}


void set_value(unsigned int value) {
DAC0->DAT[0].DATL=(unsigned char)(value & 0xff);
DAC0->DAT[0].DATH=(unsigned char)((value>>8) & 0x0f);
}

