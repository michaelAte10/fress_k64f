/*
 * Señales.c
 *
 *  Created on: 25 oct. 2018
 *      Author: Michael
 */
#include <stdio.h>
#include "fsl_debug_console.h"
#include "MK64F12.h"


int estado = 0;
float max,min = 0;
float MAX,MIN = 0;

void maxmin(float sel,float *res){
	  switch (estado) {
	    case 0:
	      if (sel > max) {
	        max = sel;
	      }
	      else {
	        estado = 1;
	        res[0] = max;
	        min = max;
	      }
	      break;
	    case 1:
	      if (sel < min) {
	        min = sel;
	      }
	      else {
	        estado = 0;
	        res[1] = min;
	        max = min;
	      }
	      break;
	  }
}
