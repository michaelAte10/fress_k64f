/*
 * adc.h
 *
 *  Created on: 23 oct. 2018
 *      Author: Michael
 */

#ifndef ADC_H_
#define ADC_H_

void init(void);
int convert_sinint(void);
void convert_conint(void);

#endif /* ADC_H_ */
