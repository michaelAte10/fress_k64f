/*
 * adc.c
 *
 *  Created on: 30/10/2018
 *      Author: vatin
 */

#include "MK64F12.h"

void adc_init(void) {
	SIM->SCGC6 |= SIM_SCGC6_ADC0(1);
	ADC0->CFG1  = ADC_CFG1_ADICLK(0) | ADC_CFG1_ADIV(3) | ADC_CFG1_ADLSMP(1) | ADC_CFG1_MODE(3);
}

void adc_convert(void) {
	ADC0->SC1[0]=0;		//convertir canal 0
}

int read_result(void) {
	return ADC0->R[0];
}
