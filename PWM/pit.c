/*
 * pit.c
 *
 *  Created on: 30/10/2018
 *      Author: vatin
 */

#include "MK64F12.h"
#include "banderas.h"

void init_PIT(void) {
	// turn on PIT
	SIM->SCGC6 |= SIM_SCGC6_PIT(1);
	PIT->MCR = 0x00;
	// Timer 1
	PIT->CHANNEL[0].LDVAL = 210000; // aprox 10 ms a 21 Mhz
	PIT->CHANNEL[0].TCTRL = PIT_TCTRL_TIE_MASK; //
	PIT->CHANNEL[0].TCTRL |= PIT_TCTRL_TEN_MASK; //
	NVIC_EnableIRQ(PIT0_IRQn);

}

int timer=0;

__attribute__((interrupt)) void PIT0_IRQHandler(void) {
	PIT->CHANNEL[0].TFLG |= PIT_TFLG_TIF_MASK;
	bandera_PIT_INT=1;
	timer++;
	if (timer>=100) {
		bandera_PIT_UNSEG=1;
		timer=0;
	}
}
