/*
 * banderas.h
 *
 *  Created on: 30/10/2018
 *      Author: vatin
 */

#ifndef BANDERAS_H_
#define BANDERAS_H_

struct Banderas{
	int b0:1;
	int b1:1;
	int b2:1;
	int resultado_IC;
	int delta;
};

extern struct Banderas banderas;

#define bandera_PIT_INT			banderas.b0
#define bandera_PIT_UNSEG		banderas.b1
#define ICapture				banderas.resultado_IC
#define ICdelta					banderas.delta

#endif /* BANDERAS_H_ */
