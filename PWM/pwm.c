/*
 * pwm.c
 *
 *  Created on: 30/10/2018
 *      Author: vatin
 */


#include "MK64F12.h"
#include "banderas.h"


//para el K64 es FTM0 ch2 en el pin ptc3 ALT4
//para el K25 es TPM0 ch4 en el pin ptc8 ALT3
//para el k27 es TPM0 ch5 en el pin ptc9  (puede ser el mismo en el kl25) ALT3
//kl46 idem para el k25 o k27

#define INPUTCAPTURE 	0

void init_pwm(void) {
	SIM->SCGC6 |= SIM_SCGC6_FTM0(1);		//TPM para kl25,kl27,kl46
	SIM->SCGC5|= SIM_SCGC5_PORTC_MASK;
	PORTC->PCR[3] = PORT_PCR_MUX(4);		//para K64
	FTM0->CONF = FTM_CONF_BDMMODE(3);		//permite funcionar en DEBUG
	FTM0->MOD=55000;
	FTM0->SC=FTM_SC_TOIE(1) | FTM_SC_CLKS(1) | FTM_SC_PS(2);
	FTM0->CONTROLS[2].CnSC = FTM_CnSC_CHIE(1) | FTM_CnSC_ELSB(1) | FTM_CnSC_ELSA(0) | FTM_CnSC_MSB(1) | FTM_CnSC_MSA(0);  //1010 PWM EDGE, High true pulses
	asm("nop");
	FTM0->CONTROLS[2].CnV = 27500;		//50% duty cycle
}


void init_inputcapture(void) {
	SIM->SCGC6 |= SIM_SCGC6_FTM0(1);		//TPM para kl25,kl27,kl46
	PORTC->PCR[3] = PORT_PCR_MUX(4);		//para K64
	FTM0->CONF = FTM_CONF_BDMMODE(3);		//permite funcionar en DEBUG
	FTM0->MOD=55000;
	FTM0->SC=FTM_SC_TOIE(1) | FTM_SC_CLKS(1) | FTM_SC_PS(7);
	FTM0->CONTROLS[2].CnSC = FTM_CnSC_CHIE(1) | FTM_CnSC_ELSB(0) | FTM_CnSC_ELSA(1) | FTM_CnSC_MSB(0) | FTM_CnSC_MSA(0);  //0001 Input cApture, flanco subida

}


void cambiar_PWM(int valor) {
	FTM0->CONTROLS[2].CnV = valor;
}


int anterior;

__attribute__((interrupt)) void FTM0_IRQHandler(void) {
	if ((FTM0->SC & FTM_SC_TOF_MASK)!=0) { //interrupcion de periodo
		FTM0->SC;
		FTM0->SC &= (~FTM_SC_TOF_MASK);

	}
	if ((FTM0->CONTROLS[2].CnSC & FTM_CnSC_CHF_MASK)!=0) { //interrupcion de periodo
		FTM0->CONTROLS[2].CnSC;
		FTM0->CONTROLS[2].CnSC &= (~FTM_CnSC_CHF_MASK);
#if INPUTCAPTURE
		anterior=ICapture;
		ICapture=FTM0->CONTROLS[2].CnV;
		if (ICapture>anterior) {
			ICdelta=ICapture-anterior;
		}
#endif
	}
}



