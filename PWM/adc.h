/*
 * adc.h
 *
 *  Created on: 30/10/2018
 *      Author: vatin
 */

#ifndef ADC_H_
#define ADC_H_

void adc_init(void);
void adc_convert(void);
int read_result(void);

#endif /* ADC_H_ */
