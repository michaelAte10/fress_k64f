
#include <stdio.h>
#include "board.h"
#include "peripherals.h"
#include "pin_mux.h"
#include "clock_config.h"
#include "MK64F12.h"
#include "fsl_debug_console.h"
#include "GPIOC.h"
#include "Teclado.h"
#include "Lcd16x2.h"
#include "GPIOB.h"
#include "Pit.h"
#include "math.h"
#include "Inter.h" // Interrupcion de pulsador de dinero
#include "Seconds.h"// RTC

/*

 */
int main(void) {

  	/* Init board hardware. */
    BOARD_InitBootPins();
    BOARD_InitBootClocks();
    BOARD_InitBootPeripherals();
  	/* Init FSL debug console. */
    BOARD_InitDebugConsole();

    init_pit_timer(); // Inicializamos PIT
    init_rtc(); // Inicializamos modulo RTC
    Init_teclado(3,2,11,10,4,12,5,7); // F1-F4,C1-C4 OUTPUT,INPUT gpioc
    Init_lcd(19,18,9,11,10,3,2); // Rs,Rw,E,d4- d7 gpiob
    Init_interrupt(0);// PortC0

    modoPinC(1,0);//PortC1 input ->PROGRAMAR

    /* Force the counter to be placed into memory. */
    volatile static int i = 0 ;

    unsigned  cantidadMoney;

    // Cada vector se respeta el espacio de la ultima POSICION = NULL //
    int posprecio[3],pospass[3];
    int Pseleccion; // precio de producto seleccionado
    int Cafavor; // creditos a favor (devuelta)
    int cost = 0; // variable que almacena cantidad de dinero

    uint8_t k = 9;
    uint8_t MODO = 0;

    char tecla;
    char pass[7];
    char buffer[7] = "";
    char Precio[7] = "";
    char clave[7] = "123456"; // Clave por defecto


    int PRECIO[4][4] = { // Matriz de precios
    		{1000,2000,3000,4000},
			{5000,6000,7000,8000},
			{8100,8200,8300,8400},
			{9000,9100,9200,9300},
    };

	enum Dispensadora{ // Maquina de estados DISPENSADORA
        Espera,
		Dinero,
		Verificar,
		Entrega,
		Devuelta,
		Pass_Privado,
		MenuPrivado,
		Newpass,
		Newprice,
		Datos
	};
	enum Dispensadora estado = Espera ,estadoAnt = Espera;

	while(1) {
    	tecla = Tecla(posprecio);
    	cantidadMoney = money(0);
    	buffer[7] = 0; // Siempre buffer final en 0 HABIAN PROBLEMAS Y SE ESTABAN SUPERPONIENDO DATOS EN OTROS

    	if(estado != estadoAnt) { // Cada que se de transicion limpiamos
    		clear_lcd();
    		estadoAnt = estado;
    	}

    	switch(estado){
    	case Espera:
    		Escribir_lcd("MAQUINA--VENDING",0,0);
        	if(tecla != 'n'){
        		clear_lcd();
        		Escribir_lcd("Cost",1,1);
        		Caracter_lcd(tecla,5,1);
        		Escribir_lcd(":$",6,1);
        		f_a(PRECIO[posprecio[0]][posprecio[1]],buffer,0);
        		Escribir_lcd(buffer,9,1);
        	}
            // Transiciones //
        	if(cantidadMoney != 0) estado = Dinero;
        	else if(LecturaC(1) == 0) estado = Pass_Privado;
    		break;
    	case Pass_Privado:
    		Escribir_lcd("-> Digite clave!",0,0);
    		Escribir_lcd("Password:",0,1);
    		if((tecla != 'n') && ((k - 9)<6)){
        		Escribir_lcd("*",k,1);
        		pass[k-9] = tecla;
        		k++;
    		}
    		else if((k-9) == 6){
    		    sprintf(buffer,"%s",pass);
    		    for(int i = 0;i<6;i++){
    		    	if(buffer[i] != clave[i]) {
    		    		estado = Espera;
    		    		break;
    		    	}
    		    	estado = MenuPrivado;
    		    }
                k = 9;
    		}

    		break;
    	case MenuPrivado:
    		Escribir_lcd("1.PassNEW",0,0);
    		Escribir_lcd("3.Dat",10,0);
    		Escribir_lcd("4.<--",11,1);
    		Escribir_lcd("2.PriceNEW",0,1);
    		// Transiciones //
    		if(tecla == '4') estado =Espera;
    		else if(tecla == '1') estado = Newpass;
    		else if(tecla == '2') estado = Newprice;
    		else if(tecla == '3') estado = Datos;

    		break;
    	case Newpass:
    		Escribir_lcd("NEWPass:",0,0);
    	     if((tecla!='n')&&((k-9)<6)){
    	         clave[k-9] = tecla;
    	         millis(1);
    	    	 Caracter_lcd(tecla,k - 1,0);
    	         k++;
    	     }
    	     else if(k-9>=6){
    	    	 Escribir_lcd("OK baby!",0,1);
    	    	 if(millis(0) > 3) {
    	    		 k = 9;
    	    		 estado = Espera;
    	    	 }
    	     }
    		break;
    	case Newprice:
    		Escribir_lcd("Product:",0,0);
    		if(tecla != 'n'){
    			if(MODO == 0){
        			Caracter_lcd(tecla,8,0);
        			pospass[0] = posprecio[0];
        			pospass[1] = posprecio[1];
                    MODO = 1;
    			}
    			else if(MODO == 1){
    			    if((tecla >= 48) && (tecla <=57 )){
    			    	Precio[k-9] = tecla;
    	    		    sprintf(buffer,"%s",Precio);
    		    		Escribir_lcd("PRECIO:$",0,1);
    		    		Escribir_lcd(buffer,8,1);
                        k++;
    			    }
    			    else if(tecla == 'A'){
    			    	clear_lcd();
    			    	k = 9;
                        Precio[0] = Precio[1] = Precio[2] = Precio[3] = Precio[4] = Precio[5]=Precio[6] = 0; // Limpiamos el buffer
    			    	PRECIO[pospass[0]][pospass[1]] = atoi(buffer); //convertimos a entero
    			    	MODO = 0;
    			    }
    			}
    		}
    		// Transiciones //
    		if(LecturaC(1) == 0){
    			estado = Espera;
    			delay(1000); //cambiar esto :/
    		}

    		break;
    	case Datos:
    		f_a(cost,buffer,0);
            Escribir_lcd(buffer,7,1);
            Escribir_lcd("DINERO:",0,1);
            delay(2000);
            estado = Espera;
    		break;
    	case Dinero:
    		Escribir_lcd("...Procesando...",0,0);
    		Escribir_lcd("Credits:",0,1);
    		f_a(cantidadMoney,buffer,0);
    		Escribir_lcd(buffer,9,1);
    		// TRANSICION //
    		if(tecla != 'n'){
        		millis(1); // Reset
    			estado = Verificar;
    		}

    		break;
    	case Entrega:
			Escribir_lcd("...Entregando :)",0,0);
			if(millis(0) >= 3 ){
    			millis(1); //Reset
				estado = Devuelta;
			}
    		break;
    	case Verificar:
    		if(cantidadMoney >= PRECIO[posprecio[0]][posprecio[1]]) {
    			millis(1); //Reset
    			Pseleccion = PRECIO[posprecio[0]][posprecio[1]];
    			cost = Pseleccion + cost;
    			estado = Entrega;
    		}
			else {
				Escribir_lcd("Insuficiente :(",0,0);
        		if(millis(0) >= 2) {
            		millis(1); // Reset
    				estado = Dinero;
        		}
			}
    		break;
    	case Devuelta:
    		Cafavor = cantidadMoney-Pseleccion;
			Escribir_lcd("Devuelta:$",0,0);
    		f_a(Cafavor,buffer,0);
			Escribir_lcd(buffer,10,0);
			if(millis(0) >= 3) {
				money(1); // Reset Monedas
				estado = Espera;
			}
    		break;
    	}
    }
    while(1)i++;
    return 0 ;
}


