/*
 * Pit.h
 *
 *  Created on: 31 oct. 2018
 *      Author: Michael
 */

#ifndef PIT_H_
#define PIT_H_

void init_pit_timer();
void delay(long unsigned retardo);

#endif /* PIT_H_ */
