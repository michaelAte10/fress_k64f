/*
 * GPIOC.h
 *
 *  Created on: 31 oct. 2018
 *      Author: Michael
 */

#ifndef GPIOC_H_
#define GPIOC_H_

void modoPinC(int pin,short int modo);
void EscrituraC(int pin,short int estado);
int LecturaC(int pin);

#endif /* GPIOC_H_ */
