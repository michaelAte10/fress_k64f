/*
 * Teclado.c
 *
 *  Created on: 31 oct. 2018
 *      Author: Michael
 */
#include <stdio.h>
#include "fsl_debug_console.h"
#include "MK64F12.h"
#include "GPIOC.h"

#define tam 4

int filas[tam];
int columnas[tam];

#define OUTPUT 1
#define INPUT 0

char caracteres[tam][tam] = {
		{'1','2','3','A'},
		{'4','5','6','B'},
		{'7','8','9','C'},
		{'*','0','#','D'},
};

void Init_teclado(uint8_t f1,uint8_t f2,uint8_t f3,uint8_t f4,uint8_t c1,uint8_t c2,uint8_t c3,uint8_t c4){

	int auxfilas[tam] = {f1,f2,f3,f4};
	int auxcolumnas[tam] = {c1,c2,c3,c4};

    for(int i=0;i<tam;i++){
    	filas[i] = auxfilas[i];
    	columnas[i] = auxcolumnas[i];
    }
    for(int i=0;i<tam;i++){
    	modoPinC(columnas[i],INPUT);  // declaramos salidas //
    	modoPinC(filas[i],OUTPUT);     //declaramos entradas//
    }
}
char Tecla(int *pos){
	char t = 'n'; // ponemos un caso que no se de
	for(int i=0;i< tam ;i++){
		// en este for interno realizamos la multiplexacion por cada estado del for externo //
		for(int j=0;j<tam;j++){
			if(i == j)EscrituraC(filas[j],0);
			else EscrituraC(filas[j],1);
		}
		// una vez definida las variables de estado podemos leer las columnas //
        for(int k=0;k<tam;k++){
        	if(!LecturaC(columnas[k])) {
        		t = caracteres[i][k];
        		pos[0]=i;
        		pos[1]=k;
        		while(!LecturaC(columnas[k]));
        	}
        }
	}
	return t;
}


