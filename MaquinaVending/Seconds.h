/*
 * Seconds.h
 *
 *  Created on: 1 nov. 2018
 *      Author: Michael
 */

#ifndef SECONDS_H_
#define SECONDS_H_

void init_rtc();
int millis(short int reset);

#endif /* SECONDS_H_ */
