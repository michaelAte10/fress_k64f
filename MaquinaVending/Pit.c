/*
 * Pit.c
 *
 *  Created on: 31 oct. 2018
 *      Author: Michael
 */


#include <stdio.h>
#include "fsl_debug_console.h"
#include "MK64F12.h"

long unsigned ciclosReloj = 0;
long unsigned tiempo_ms = 0; // tiempo en milisegundos

void init_pit_timer(){
	SIM->SCGC6=SIM->SCGC6|SIM_SCGC6_PIT_MASK;
	PIT->MCR=PIT_MCR_FRZ_MASK|PIT_MCR_MDIS(0);
	PIT->CHANNEL[0].TCTRL=PIT_TCTRL_TEN(0)|PIT_TCTRL_TIE(1);
	PIT->CHANNEL[0].TFLG=PIT_TFLG_TIF_MASK;
	NVIC->ISER[1]=(1<<16);			// OTRA FORMA NVIC_EnableIRQ(PIT0_IRQn);
}
void delay(long unsigned retardo){
	ciclosReloj = 0;
	tiempo_ms = retardo*430;
	PIT->CHANNEL[0].TCTRL=PIT_TCTRL_TEN(1)|PIT_TCTRL_TIE(1);
}

__attribute__((interrupt))void PIT0_IRQHandler(void){
	PIT->CHANNEL[0].TFLG=PIT_TFLG_TIF_MASK;
	ciclosReloj++;
	if(ciclosReloj >= tiempo_ms)PIT->CHANNEL[0].TCTRL=PIT_TCTRL_TEN(0)|PIT_TCTRL_TIE(1);
}

