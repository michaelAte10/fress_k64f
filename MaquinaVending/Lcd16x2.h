/*
 * Lcd16x2.h
 *
 *  Created on: 31 oct. 2018
 *      Author: Michael
 */

#ifndef LCD16X2_H_
#define LCD16X2_H_

void modo(short int IO);
void write_lcd(char dato,short int rsf,short int size);
void Escribir_lcd(char *p,uint8_t pos,uint8_t fila);
void Caracter_lcd(char p,uint8_t pos,uint8_t fila);
void clear_lcd(void);
void f_a(float n, char *res, int afterpoint);
void Init_lcd(uint8_t rs,uint8_t rw,uint8_t e,uint8_t d4a,uint8_t d5a,uint8_t d6a,uint8_t d7a);
#endif /* LCD16X2_H_ */
