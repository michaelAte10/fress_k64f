/*
 * GPIOB.c
 *
 *  Created on: 31 oct. 2018
 *      Author: Michael
 */
#include <stdio.h>
#include "fsl_debug_console.h"
#include "MK64F12.h"

#define BIT(X) (1<<X) //Funcion que rota el 1 el numero de veces que diga X

void modoPinB(int pin,short int modo){
	SIM->SCGC5 |= SIM_SCGC5_PORTB_MASK; // prendemos el puerto B
    PORTB->PCR[pin] |= PORT_PCR_MUX(1); // activamos el puerto B en modo gpio
    if(modo == 1){
    	GPIOB->PDDR |= BIT(pin);  // Seleccionamos el bit pin como salida
    	GPIOB->PSOR |= BIT(pin);
    }
    else if(modo == 0){ // por defecto queda como entrada
    }
}
void EscrituraB(int pin,short int estado){
	if(estado == 0){
	    GPIOB->PCOR |= BIT(pin);
	}
	else if(estado == 1){
	    GPIOB->PSOR |= BIT(pin);
	}
}
int LecturaB(int pin){
	int aux = BIT(pin);
	aux  &= GPIOB->PDIR;
	if(aux!=0) aux = 1;
	return aux;
}


