/*
 * Teclado.h
 *
 *  Created on: 31 oct. 2018
 *      Author: Michael
 */

#ifndef TECLADO_H_
#define TECLADO_H_

void Init_teclado(uint8_t f1,uint8_t f2,uint8_t f3,uint8_t f4,uint8_t c1,uint8_t c2,uint8_t c3,uint8_t c4);
char Tecla(int *pos);

#endif /* TECLADO_H_ */
