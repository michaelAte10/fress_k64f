/*
 * Lcd16x2.c
 *
 *  Created on: 31 oct. 2018
 *      Author: Michael
 */


#include <stdio.h>
#include "fsl_debug_console.h"
#include "MK64F12.h"
#include "GPIOB.h"
#include "Pit.h"
#include "math.h"

#define OUTPUT 1
#define INPUT 0


int control[3]; // rs,rw,e
int pines[4];

void modo(short int IO){
 if(IO == 1){
    for(int i=0;i<4;i++){
    	modoPinB(pines[i],OUTPUT);  // declaramos salidas  d4,d5,d6,d7//
    }
  }
 else {
	 for(int i=0;i<4;i++){
	    modoPinB(pines[i],INPUT);  // declaramos entradas d4,d5,d6,d7//
	}
  }
}
void write_lcd(char dato,short int rsf,short int size){
	int mascara;
	int tem;
	modo(OUTPUT);
	EscrituraB(control[1],0); // indica modo de escribir
	if(rsf == 1)EscrituraB(control[0],1);// es un dato
	else if(rsf == 0)EscrituraB(control[0],0); // es un comando
	if(size == 1) mascara = 0x08;
	else if(size == 2) mascara = 0x80;
	for(int j = 0 ;j<size;j++){
		EscrituraB(control[2],1); // indica que vamos a enviar el dato a escribir en este momento
		for(int i = 0;i<4;i++){
			tem = dato & mascara;
			mascara = mascara>>1;
			EscrituraB(pines[i],(tem)?1:0);
		}
		EscrituraB(control[2],0); // indica que vamos a enviar el dato a escribir en este momento
	}
	modo(INPUT);
}
void Escribir_lcd(char *p,uint8_t pos,uint8_t fila){
	if(fila == 1) write_lcd(0b11000000 + pos,0,2);
	else if(fila == 0)write_lcd(0x80 + pos,0,2);
	while(*p){
		write_lcd(*p,1,2);
		p++; // aumentamos la direccion
	}
}
void Caracter_lcd(char p,uint8_t pos,uint8_t fila){
	if(fila == 1) write_lcd(0b11000000 + pos,0,2);
	else if(fila == 0)write_lcd(0x80 + pos,0,2);
	write_lcd(p,1,2);
}
void Init_lcd(uint8_t rs,uint8_t rw,uint8_t e,uint8_t d4a,uint8_t d5a,uint8_t d6a,uint8_t d7a){

	pines[0] = d7a;
    pines[1] = d6a;
    pines[2] = d5a;
    pines[3] = d4a;

    control[0] = rs;
    control[1] = rw;
    control[2] = e;

	modoPinB(control[0],OUTPUT); // rs
	modoPinB(control[1],OUTPUT); // rw
	modoPinB(control[2],OUTPUT); // e
	EscrituraB(control[1],0); // indica modo de escribir
	EscrituraB(control[2],0); // indica que vamos a enviar el dato a escribir en este momento
    delay(40);
	write_lcd(0x03,0,1);
	delay(15);
	write_lcd(0x03,0,1);
    delay(5);
	write_lcd(0x03,0,1);
	delay(5);
	write_lcd(0x02,0,2);
	delay(5);
	write_lcd(0b1100,0,2);
	delay(1);
	write_lcd(0x06,0,2);
	write_lcd(0x80,0,2);
    write_lcd(0b00000001,0,2); //clear
    delay(100);
}
void clear_lcd(void){
    write_lcd(0b00000001,0,2); //clear
	EscrituraB(control[1],1); // indica modo de escribir
    while(LecturaB(pines[0]) != 0);
    delay(2);
}
void f_a(float n, char *res, int cifras)
{
    //sacamos la parte entera
	char numdeci[cifras];
    int ipart = (int)n;
    // sacamos la parte flotante
    float fpart = n - (float)ipart;
    int i_fpart;
    sprintf(res,"%d",ipart);
    if (cifras != 0)
    {
        strcat(res,"."); // lo concatenamos con el .
        fpart = fpart * pow(10, cifras); // buscamos su parte entera
        i_fpart = abs((int)fpart); // sacamos su parte entera
        sprintf(numdeci,"%d",i_fpart);// lo volvemos una cadena
        strcat(res,numdeci);//concatenamos ambas cadenas
    }
}
