/*
 * GPIOB.h
 *
 *  Created on: 31 oct. 2018
 *      Author: Michael
 */

#ifndef GPIOB_H_
#define GPIOB_H_

void modoPinB(int pin,short int modo);
void EscrituraB(int pin,short int estado);
int LecturaB(int pin);

#endif /* GPIOB_H_ */
