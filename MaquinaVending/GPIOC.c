/*
 * GPIOC.c
 *
 *  Created on: 31 oct. 2018
 *      Author: Michael
 */

#include <stdio.h>
#include "fsl_debug_console.h"
#include "MK64F12.h"

#define BIT(X) (1<<X) //Funcion que rota el 1 el numero de veces que diga X

void modoPinC(int pin,short int modo){
	SIM->SCGC5 |= SIM_SCGC5_PORTC_MASK; // prendemos el puerto C
    PORTC->PCR[pin] |= PORT_PCR_MUX(1); // activamos el puerto C en modo gpio
    if(modo == 1){
    	GPIOC->PDDR |= BIT(pin);  // Seleccionamos el bit pin como salida
    	GPIOC->PSOR |= BIT(pin);
    }
    else if(modo == 0){ // por defecto queda como entrada
    }
}
void EscrituraC(int pin,short int estado){
	if(estado == 0){
	    GPIOC->PCOR |= BIT(pin);
	}
	else if(estado == 1){
	    GPIOC->PSOR |= BIT(pin);
	}
}
int LecturaC(int pin){
	int aux = BIT(pin);
	aux  &= GPIOC->PDIR;
	if(aux!=0) aux = 1;
	//printf("%d\n",aux);
	return aux;
}



