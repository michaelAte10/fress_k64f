/*
 * Inter.h
 *
 *  Created on: 31 oct. 2018
 *      Author: Michael
 */

#ifndef INTER_H_
#define INTER_H_

void Init_interrupt(short int pin);
unsigned long money(uint8_t reset);

#endif /* INTER_H_ */
