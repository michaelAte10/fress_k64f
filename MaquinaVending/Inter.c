/*
 * Inter.c
 *
 *  Created on: 31 oct. 2018
 *      Author: Michael
 */

#include <stdio.h>
#include "fsl_debug_console.h"
#include "MK64F12.h"
unsigned long dinero = 0;
short int pinaux;
void Init_interrupt(short int pin){
    pinaux = pin;
	SIM->SCGC5 |= SIM_SCGC5_PORTC_MASK; // prendemos el puerto C
    PORTC->PCR[pin] = PORT_PCR_MUX(1) | PORT_PCR_IRQC(10)|PORT_PCR_ISF_MASK | PORT_PCR_PE_MASK | PORT_PCR_PS_MASK; // PULL-UP Y FALLING
	NVIC_EnableIRQ(PORTC_IRQn); // Habilitacion de interrupcion
}
unsigned long money(uint8_t reset){
	if(reset == 1) dinero = 0;
	return dinero;
}

__attribute__((interrupt)) void PORTC_IRQHandler(void)
{
	PORTC->PCR[pinaux] |= PORT_PCR_ISF_MASK;
	dinero = dinero + 100;
}
