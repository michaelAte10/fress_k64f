/*
 * The Clear BSD License
 * Copyright (c) 2015, Freescale Semiconductor, Inc.
 * Copyright 2016-2017 NXP
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted (subject to the limitations in the disclaimer below) provided
 *  that the following conditions are met:
 *
 * o Redistributions of source code must retain the above copyright notice, this list
 *   of conditions and the following disclaimer.
 *
 * o Redistributions in binary form must reproduce the above copyright notice, this
 *   list of conditions and the following disclaimer in the documentation and/or
 *   other materials provided with the distribution.
 *
 * o Neither the name of the copyright holder nor the names of its
 *   contributors may be used to endorse or promote products derived from this
 *   software without specific prior written permission.
 *
 * NO EXPRESS OR IMPLIED LICENSES TO ANY PARTY'S PATENT RIGHTS ARE GRANTED BY THIS LICENSE.
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "board.h"
#include "fsl_uart.h"

#include "pin_mux.h"
#include "clock_config.h"

#include "Pit.h"
#include "GPIOB.h"
#include "one_wire.h"
#include "adc.h"

#include  "math.h"
/*******************************************************************************
 * Definitions
 ******************************************************************************/
/* UART instance and clock */
#define DEMO_UART UART4
#define DEMO_UART_CLKSRC UART4_CLK_SRC
#define DEMO_UART_CLK_FREQ CLOCK_GetFreq(UART4_CLK_SRC)
#define DEMO_UART_IRQn UART4_RX_TX_IRQn
#define DEMO_UART_IRQHandler UART4_RX_TX_IRQHandler


/*! @brief Ring buffer size (Unit: Byte). */
#define DEMO_RING_BUFFER_SIZE 16

/*! @brief Ring buffer to save received data. */

/*******************************************************************************
 * Prototypes
 ******************************************************************************/
void f_a(float n, char *res, int cifras);

/*******************************************************************************
 * Variables
 ******************************************************************************/

uint8_t g_tipString[] =
    "Uart functional API interrupt example\r\nBoard receives characters then sends them out\r\nNow please input:\r\n";

/*
  Ring buffer for data input and output, in this example, input data are saved
  to ring buffer in IRQ handler. The main function polls the ring buffer status,
  if there are new data, then send them out.
  Ring buffer full: (((rxIndex + 1) % DEMO_RING_BUFFER_SIZE) == txIndex)
  Ring buffer empty: (rxIndex == txIndex)
*/
uint8_t demoRingBuffer[DEMO_RING_BUFFER_SIZE];
volatile uint16_t txIndex; /* Index of the data to send out. */
volatile uint16_t rxIndex; /* Index of the memory to save new arrived data. */

/*******************************************************************************
 * Code
 ******************************************************************************/

void DEMO_UART_IRQHandler(void)
{
    uint8_t data;

    /* If new data arrived. */
    if ((kUART_RxDataRegFullFlag | kUART_RxOverrunFlag) & UART_GetStatusFlags(DEMO_UART))
    {
        data = UART_ReadByte(DEMO_UART);

        /* If ring buffer is not full, add data to ring buffer. */
        if (((rxIndex + 1) % DEMO_RING_BUFFER_SIZE) != txIndex)
        {
            demoRingBuffer[rxIndex] = data;
            rxIndex++;
            rxIndex %= DEMO_RING_BUFFER_SIZE;
        }
    }
    /* Add for ARM errata 838869, affects Cortex-M4, Cortex-M4F Store immediate overlapping
      exception return operation might vector to incorrect interrupt */
#if defined __CORTEX_M && (__CORTEX_M == 4U)
    __DSB();
#endif
}

/*!
 * @brief Main function
 */
int main(void)
{
    uart_config_t config;

    BOARD_InitPins();
    BOARD_BootClockRUN();

    /*
     * config.baudRate_Bps = 115200U;
     * config.parityMode = kUART_ParityDisabled;
     * config.stopBitCount = kUART_OneStopBit;
     * config.txFifoWatermark = 0;
     * config.rxFifoWatermark = 1;
     * config.enableTx = false;
     * config.enableRx = false;
     */
    init_PIT();
    init_ADC();
    Init_OneWire(3);
    modoPinB(3,0); // Input


    UART_GetDefaultConfig(&config);
    config.baudRate_Bps = BOARD_DEBUG_UART_BAUDRATE;
    config.enableTx = true;
    config.enableRx = true;

    UART_Init(DEMO_UART, &config, DEMO_UART_CLK_FREQ);

    /* Send g_tipString out. */
    //UART_WriteBlocking(DEMO_UART, g_tipString, sizeof(g_tipString) / sizeof(g_tipString[0]));

    /* Enable RX interrupt. */
    UART_EnableInterrupts(DEMO_UART, kUART_RxDataRegFullInterruptEnable | kUART_RxOverrunInterruptEnable);
    EnableIRQ(DEMO_UART_IRQn);

    unsigned tiempo[3];
    char buff_tem[8];
    char txBuff[8];

    float dataSensor;

    //UART_WriteBlocking(DEMO_UART,"JUUJ",11);
    //UART_WriteBlocking(DEMO_UART,"JDJSJDS",11);
    //UART_WriteBlocking(DEMO_UART,"JHIDIJOSDHJ",11);
    UART_WriteBlocking(DEMO_UART,"*HB=21.23",9);
    UART_WriteBlocking(DEMO_UART,"*HB=25.92",9);
    UART_WriteBlocking(DEMO_UART,"*HB=28.92",9);

    //UART_WriteBlocking(DEMO_UART,"JHIDIJOSDHJ",11);
  //  UART_WriteBlocking(DEMO_UART,"*BT=4",5);
   // UART_WriteBlocking(DEMO_UART,"*BT=2",5);



    while (1)
    {
    	timer_variables(tiempo);
    	convert_t();
        read_scratchpad(buff_tem);


        dataSensor = dataTem(buff_tem);
        f_a(dataSensor, buff_tem, 2);

        sprintf(txBuff,"*HB=%s",buff_tem);
	   // UART_WriteBlocking(DEMO_UART,txBuff,6);
	   // UART_WriteBlocking(DEMO_UART,txBuff,6);

    	if(tiempo[1] >= 2){
    	   UART_WriteBlocking(DEMO_UART,txBuff,9);
    	    reset_time();
    	}
        /* Send data only when UART TX register is empty and ring buffer has data to send out. */
      //  while ((kUART_TxDataRegEmptyFlag & UART_GetStatusFlags(DEMO_UART)) && (rxIndex != txIndex))
      //  {
      //      UART_WriteByte(DEMO_UART, demoRingBuffer[txIndex]);
      //      txIndex++;
      //      txIndex %= DEMO_RING_BUFFER_SIZE;
      //  }
    }
}
void f_a(float n, char *res, int cifras)
{
    //sacamos la parte entera
	char numdeci[cifras];
    int ipart = (int)n;
    // sacamos la parte flotante
    float fpart = n - (float)ipart;
    int i_fpart;
    sprintf(res,"%d",ipart);
    if (cifras != 0)
    {
        strcat(res,"."); // lo concatenamos con el .
        fpart = fpart * pow(10, cifras); // buscamos su parte entera
        i_fpart = abs((int)fpart); // sacamos su parte entera
        sprintf(numdeci,"%d",i_fpart);// lo volvemos una cadena
        strcat(res,numdeci);//concatenamos ambas cadenas
    }
}
