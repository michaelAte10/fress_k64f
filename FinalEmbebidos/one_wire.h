/*
 * one_wire.h
 *
 *  Created on: 24 abr. 2019
 *      Author: Michael
 */

#ifndef ONE_WIRE_H_
#define ONE_WIRE_H_

/*
 * one_wire.c
 *
 *  Created on: 24 abr. 2019
 *      Author: Michael
 */


#include <stdio.h>
#include "fsl_debug_console.h"
#include "MK64F12.h"
#include "GPIOB.h"


void Init_OneWire(int pin);
void low(int us);
int reset(void);
void send_bit(int bit);
int read_bit(void);
void send_byte(char msg);
char read_byte(void);
unsigned char CRC8(char *data, int len);
void ROM(void);
void convert_t (void);
void read_scratchpad(char *buff);
float dataTem(char *data);

#endif /* ONE_WIRE_H_ */
