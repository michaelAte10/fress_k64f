/*
 * Seconds.h
 *
 *  Created on: 1 nov. 2018
 *      Author: Michael
 */

#ifndef SECONDS_H_
#define SECONDS_H_

void init_rtc();
/**
 * Esta funcion es un contador de tiempo en segundos que se retorna
 * @param reset cuando es 1 se restable el tiempo a 0
 */
unsigned seconds(short int reset);

#endif /* SECONDS_H_ */
