/*
 * one_wire.c
 *
 *  Created on: 24 abr. 2019
 *      Author: Michael
 */


#include <stdio.h>
#include "fsl_debug_console.h"
#include "MK64F12.h"
#include "GPIOB.h"
#include "Pit.h"

#define input 0
#define output 1

#define TREC 2 // uS tiempo de recuperacion
#define TIMESLOP 90// tiempo entre cada lectura

int pinGlobal;
void Init_OneWire(int pin){
	pinGlobal = pin; // Para solo delclaralo una vez
	init_PIT();
}

void low(int us){
	modoPinB(pinGlobal, output);
	EscrituraB(pinGlobal, 0); // bajamos el pin
    delay(us);
	modoPinB(pinGlobal, input); // lo devolvemos a entrada
	delay(TREC);
}
int reset(void){
	int v;
	low(960); // vamos a preguntar si hay esclavos
	delay(960);
	v = LecturaB(pinGlobal); // leemos la respuesta del esclavo
	delay(300);
	return !v;
}
void send_bit(int bit){
	// si ponemos el pin en 0 durante un tiempo menor a 15 us estamos enviando un uno , sino un cero
	if(bit){
		low(2);
		delay(TIMESLOP-5);
	}else{
		low(TIMESLOP);
	}
}
int read_bit(void){
	int r;
	low(2);
	r = LecturaB(pinGlobal);
	delay(TIMESLOP-5);
	return r;
}
void send_byte(char msg){
	int mascara = 1;
	int msg_aux = 0;
	int var;
	for (var = 0; var < 8; ++var) {
		msg_aux = msg & mascara;
		send_bit(msg_aux);
		msg = msg>>1;
	}
}
char read_byte(void){
	int byte = 0;
	int bit = 0;
	int var;
	for (var = 0; var < 8; ++var) {
		bit = read_bit();
		bit = bit << var;
		byte = byte | bit;
	}
	return byte;
}
unsigned char CRC8(char *data, int len)
{
    unsigned char crc = 0x00;

    while (len--)
    {
        unsigned char extract = *data++;

        for (unsigned char tempI = 8; tempI; tempI--)
        {
            unsigned char sum = (crc ^ extract) & 0x01;
            crc >>= 1;

            if (sum)
            {
                crc ^= 0x8C;
            }

            extract >>= 1;
        }
    }

  return crc;
}

void ROM(void){
	char rom[8];
	send_byte(0x33); // con este comando preguntamos la rom
	int var;
	printf("REGISTRO ROM...\n");
	for (var = 0; var < 8; ++var) {
		rom[var] = read_byte();
		printf("%x\t",rom[var]);
	}
	printf("ok \n");
	reset();
}
void convert_t (void) {
 send_byte(0xCC);
 send_byte(0x44);

 while (read_byte() != 0xFF);
 delay(20000);
 reset();
}
void read_scratchpad(char *buff) {
 send_byte(0xCC);
 send_byte(0xBE);

 int i;
 //printf("REGISTRO TEM...\n");
 for (i = 0; i < 9; i++) {
  buff[i] = read_byte();
 //printf("%d\t ", buff[i]);
 }
 reset();
}
float dataTem(char *data){
	uint16_t raw = (data[1]<<8)|data[0];
    float tem = (float)(raw/16.0);
    return tem;
}


