/**
 * Seconds.c
 *@file Seconds.h Esta libreria es capaz de llevar un conteo en segundos , la usamos para realizar temporizadores
 *  Created on: 1 nov. 2018
 *      Author: Michael
 */

#include <stdio.h>
#include "fsl_debug_console.h"
#include "MK64F12.h"

unsigned tiempo = 0; // tiempo inicial
/**
 * Esta funcion se encarga de inicializar el modulo RTC
 */
void init_rtc(){
	SIM->SCGC6 |= SIM_SCGC6_RTC_MASK; // Prendemos modulo clock
	RTC->SR  =  ~RTC_SR_TCE_MASK; // bandera deshabilitamos interrupcion
	RTC->CR  =  RTC_CR_OSCE_MASK;// activamos
	RTC->TSR = 1;// donde inicia el tiempo
	RTC->IER =  RTC_IER_TSIE_MASK; //
	RTC->SR  =  RTC_SR_TCE_MASK; // bandera habilitar interrupcion
	NVIC_EnableIRQ(RTC_Seconds_IRQn); // Habilitacion de interrupcion de segundos
}
/**
 * Esta funcion es un contador de tiempo en segundos que se retorna
 * @param reset cuando es 1 se restable el tiempo a 0
 */
unsigned seconds(short int reset){
	if(reset == 1) tiempo = 0;
	return tiempo;
}
__attribute__((interrupt)) void RTC_Seconds_IRQHandler(void){
	tiempo++;
}

