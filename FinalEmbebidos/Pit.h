/*
 * Pit.h
 *
 *  Created on: 26 abr. 2019
 *      Author: Michael
 */

#ifndef PIT_H_
#define PIT_H_

void init_PIT(void);
void delay(unsigned tiempo_us);
void timer_variables(unsigned *tiempos);
void reset_time(void);

#endif /* PIT_H_ */
