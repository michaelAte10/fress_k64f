/*
 * Pit.c
 *
 *  Created on: 26 abr. 2019
 *      Author: Michael
 */

#include <stdio.h>
#include "fsl_debug_console.h"
#include "MK64F12.h"

void init_PIT(void) {
	// turn on PIT
	SIM->SCGC6 |= SIM_SCGC6_PIT(1);
	PIT->MCR = 0x00;

	// Timer 1
	PIT->CHANNEL[0].LDVAL = 60; // aprox 1 us a 120 Mhz
	PIT->CHANNEL[0].TCTRL = PIT_TCTRL_TIE_MASK; //
	PIT->CHANNEL[0].TCTRL |= PIT_TCTRL_TEN_MASK; //

	// Time 2 //
	PIT->CHANNEL[1].LDVAL = 60000; // aprox 1 ms a 120 Mhz
	PIT->CHANNEL[1].TCTRL = PIT_TCTRL_TIE_MASK; //
	PIT->CHANNEL[1].TCTRL |= PIT_TCTRL_TEN_MASK; //

	NVIC_EnableIRQ(PIT0_IRQn);
	NVIC_EnableIRQ(PIT1_IRQn);
}


int timer=0;
int timeGlobal = 0;
unsigned U_Second = 0,U_Millis = 0;
int bandera = 0;
void delay(unsigned tiempo_us){
	volatile int i = 0;
	timeGlobal = tiempo_us;
	PIT->CHANNEL[0].TCTRL=PIT_TCTRL_TEN(1)|PIT_TCTRL_TIE(1);
	while(!bandera){
		i++;
	}
	PIT->CHANNEL[0].TCTRL=PIT_TCTRL_TEN(0)|PIT_TCTRL_TIE(1);
	bandera = 0;
}
void timer_variables(unsigned *tiempos){
	tiempos[0] = U_Millis;
	tiempos[1] = U_Second;
}
void reset_time(void){
	U_Millis = 0;
	U_Second = 0;
}
__attribute__((interrupt)) void PIT0_IRQHandler(void) {
	PIT->CHANNEL[0].TFLG |= PIT_TFLG_TIF_MASK;
	timer++;
	if (timer>=timeGlobal) {
		bandera = 1;
		timer=0;
	}
}
__attribute__((interrupt)) void PIT1_IRQHandler(void) {
	U_Millis++;
	if(U_Millis >= 1000){
		U_Second++;
		U_Millis = 0;
		//printf("%d\n",U_Second);
	}
	PIT->CHANNEL[1].TFLG |= PIT_TFLG_TIF_MASK;
}
