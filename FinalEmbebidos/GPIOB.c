/**
 * GPIOB.c
 * @file GPIOB.h Esta Libreria consta de 3 funciones que son para el uso GPIO del puerto B
 * Created on: 31 oct. 2018
 * Author: Michael
 */


#include <stdio.h>
#include "fsl_debug_console.h"
#include "MK64F12.h"
#include "GPIOB.h"

#define BIT(X) (1 << X) //Funcion que rota el 1 el numero de veces que diga X

/**
 * Esta funcion selecciona algun pin del puertoB y se encarga de ponerlo como entrada o salida
 * @param pin selecciona que pin del puertoB se desea escoger
 * @param modo escoge si el pin es como entrada(0) o como salida(1)
 */
void modoPinB(int pin,short int modo){ // @ Le ingresas
	SIM->SCGC5 |= SIM_SCGC5_PORTB_MASK; // prendemos el puerto B
    PORTB->PCR[pin] |= PORT_PCR_MUX(1); // activamos el puerto B en modo gpio
    if(modo == 1){
    	GPIOB->PDDR |= BIT(pin);  // Seleccionamos el bit pin como salida
    	//GPIOB->PSOR |= BIT(pin);
    }
    else if(modo == 0){ // por defecto queda como entrada
    	GPIOB->PDDR &= (!BIT(pin));
    }
}
/**
 * La funcion EscrituraB se encarga de activar o desactivar el pin(del puertoB) que escogimos como salida
 * @param pin el pin que se desea manipular
 * @param estado modo del pin HIGH - LOW
 */
void EscrituraB(int pin,short int estado){
	if(estado == 0){
	    GPIOB->PCOR |= BIT(pin);
	}
	else if(estado == 1){
	    GPIOB->PSOR |= BIT(pin);
	}
}
/**
 * Esta funcion se encarga de leer el pin deseado del puertoB y retornarlo
 * @param pin el pin del puertoB que se desea leer
 */
int LecturaB(int pin){
	int aux = BIT(pin);
	aux  &= GPIOB->PDIR;
	if(aux!=0) aux = 1;
	return aux;
}


